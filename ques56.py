with open('file1.txt', 'r') as rf:
    try:
        rf.write('Write Me as a Text!')
    except IOError:
        print('Writing File opened in Read Mode only')

    try:
        print('Converting "abc" to integer')
        abc = int('abc')
    except ValueError:
        print('Cannot do the conversion')

