with open('file1.txt', 'r') as rf:
    content = rf.read(10)

    while len(content) > 0:
        print('Actual Read Data : ', content)
        print('File Pointer Position : ', rf.tell())
        print('-' * 30)

        content = rf.read(10)

print('-' * 30)

"""
Reset the file pointer after reading 100 Character from file 
(Use Seek function to reset)

"""

with open('file1.txt', 'r') as rf:
    content = rf.read(100)
    print(content)
    rf.seek(0)


"""
Open the file in read mode and start printing the contents from 5th line onwards.

"""
print('-' * 30)

with open('file1.txt', 'r') as rf:
    i = 1
    for line in rf:
        if i >= 5:
            print(f'Line : {i} : ', line, end='')
        i += 1

