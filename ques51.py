import glob
import re

file_name_list = glob.glob('*.txt')
mydict = {}

for i in file_name_list:
    with open(i, 'r') as rf:
        count = re.findall('Treasure', rf.read())
        if len(count) > 0:
            mydict[i] = len(count)


print('Number of file(s) containing the pattern : ', len(mydict))

for key, item in mydict.items():
    print(f'File Name : {key}, Pattern Repetition Time(s) : {item}')
