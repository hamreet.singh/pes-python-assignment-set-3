with open('file1.txt', 'r') as rf:
    content = rf.readlines()
    for i in content[::-1]:
        print(i, end='')

print('--' * 35)



with open('file1.txt', 'r') as rf:
    content = rf.read()
    print(content[::-1])
