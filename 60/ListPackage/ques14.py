A = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10']

B = ['Jon', 'Bill', 'Maria', 'Jenny', 'Jack', 'Joseph', 'Anthony', 'Decker', 'Lucifer', 'Chole']

print('Employee names : ', end='')
for i in B:
    print(i, end=', ')

index = int(input('\nEnter the index value to read : '))

print('Employee Name : ', B[index])
print('Employee Id : ', A[index])

print('Name from 4th to 9th Position : ', B[4:10])
print('Name from 3rd to end position : ', B[3:])

N = int(input('Enter the value of N : '))

print('Repeating list : ', A * N, B * N)

new_list = A[:]
new_list.extend(B)

print('Concatinating both list : ', new_list)

print('ID Employee')
for i in range(len(A)):
    print(A[i], B[i])
