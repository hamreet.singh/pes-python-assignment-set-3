import random

list1 = [random.randint(1, 100) for i in range(10)]
list2 = [random.randint(1, 100) for i in range(10)]
list3 = [random.randint(1, 100) for i in range(10)]

print('List 1 : ', list1)
print('List 2 : ', list2)
print('List 3 : ', list3)

Maxlist = []

list1.sort()
list2.sort()
list3.sort()

Maxlist.extend(list1[-2:])
Maxlist.extend(list2[-2:])
Maxlist.extend(list3[-2:])

print('Max List : ', Maxlist)
print('Averge Value of Maxlist : %.2f' % (sum(Maxlist) / len(Maxlist)))

Minlist = []

Minlist.extend(list1[:2])
Minlist.extend(list2[:2])
Minlist.extend(list3[:2])

print('Min List : ', Minlist)
print('Averge Value of Minlist : %.2f' % (sum(Minlist) / len(Minlist)))
