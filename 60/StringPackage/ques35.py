tup1 = ('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday')

print('Days in a week : ', end='')
for i in tup1:
    print(i, end=', ')

tup2 = ('January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December')

print('\nMonths in a year : ', end='')
for i in tup2:
    print(i, end=', ')
print()

temp = list(tup1)
temp.extend(list(tup2))

print('Concatinating Both the Tuples : ', tuple(temp))

import random

tup1 = tuple([random.randint(1, 100) for i in range(10)])
tup2 = tuple([random.randint(1, 100) for i in range(10)])
tup3 = tuple([random.randint(1, 100) for i in range(10)])

print('Tuple 1 : ', tup1)
print('Tuple 2 : ', tup2)
print('Tuple 3 : ', tup3)


print('Greater in Tuple 1 : ', max(tup1))
print('Greater in Tuple 2 : ', max(tup2))
print('Greater in Tuple 3 : ', max(tup3))


try:
    del tup1[0]
    print('Tuple 1 : ', tup1)
except Exception as exp:
    print('Type Error : ', exp)

try:
    del tup1
    print('Tuple 1 : ', tup1)
except Exception as exp:
    print('Name Error : ', exp)


tup1 = tuple([random.randint(1, 100) for i in range(10)])

print('Tuple 1 : ', tup1)

tup1 = list(tup1)

tup1.append(50000)
tup1 = tuple(tup1)

print('Appending 50000, New Tuple 1 : ', tup1)
