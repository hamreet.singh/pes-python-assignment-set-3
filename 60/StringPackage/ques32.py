def get_max_min(sample_list):
    l = len(sample_list[0])
    s = len(sample_list[0])

    largest_city = sample_list[0]
    smallest_city = sample_list[0]

    for i in sample_list:
        if len(i) > l:
            l = len(i)
            largest_city = i

        elif len(i) < s:
            s = len(i)
            smallest_city = i

    return l, s, largest_city, smallest_city


def display_list(sample_list):
    for i in sample_list:
        print(i, '\t\t', len(i))


list1 = ['Mumbai', 'Delhi', 'Bangalore', 'Hyderabad', 'Ahmedabad']
list2 = ['Chennai', 'Kolkata', 'Surat', 'Pune', 'Jaipur']
list3 = ['Lucknow', 'Visakhapatnam', 'Thane', 'Kanpur', 'Nagpur']

print('City Name\tLength')
display_list(list1)
display_list(list2)
display_list(list3)

if __name__ == '__main__':
    large1, small1, l_city1, s_city1 = get_max_min(list1)
    print(f'Largest Value in list1 : {large1} and Smallest Value is {small1}')

    large2, small2, l_city2, s_city2 = get_max_min(list2)
    print(f'Largest Value in list2 : {large2} and Smallest Value is {small2}')

    large3, small3, l_city3, s_city3 = get_max_min(list3)
    print(f'Largest Value in list3 : {large3} and Smallest Value is {small3}')

    if large1 > large2 and large1 > large3:
        print(f'Largest City : "{l_city1}" of length : {large1}')
    elif large2 > large3:
        print(f'Largest City : "{l_city2}" of length : {large2}')
    else:
        print(f'Largest City : "{l_city3}" of length : {large3}')

    if small1 < small2 and small1 < small3:
        print(f'Samllest City : "{s_city1}" of length : {small1}')
    elif small2 > small3:
        print(f'Smallest City : "{s_city2}" of length : {small2}')
    else:
        print(f'Smallest City : "{s_city3}" of length : {small3}')

    list1 = list1[1:-1]
    list2 = list2[1:-1]
    list3 = list3[1:-1]

    print('List 1 : ', list1)
    print('List 2 : ', list2)
    print('List 3 : ', list3)


