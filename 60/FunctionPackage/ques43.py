import random


def search(mylist, *args):
    for i in args:
        flag = 0
        for j in mylist:
            if i == j:
                flag = 1
                break

        if flag == 1:
            print(f'{i} ---> Found')
        else:
            print(f'{i} ---> Not Found')


generate_list = []

for i in range(10):
    generate_list.append(random.randint(1, 10))

print('Generated List : ', generate_list)

num1 = random.randint(1, 10)
num2 = random.randint(1, 10)

print(f'Searching for {num1} and {num2}')

search(generate_list, num1, num2)
