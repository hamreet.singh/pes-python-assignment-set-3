import random


def add(num1, num2):
    return num1 + num2


def sub(num1, num2):
    return num1 - num2


def mul(num1, num2):
    return num1 * num2


def div(num1, num2):
    return num1 / num2


def square_root(num1):
    return num1 ** 0.5


def create_substring(string, delimiter):
    w = ''
    string += delimiter
    substrlist = []

    for i in string:
        if i == delimiter:
            substrlist.append(w)
            w = ''
        else:
            w += i

    return substrlist


A = random.randint(1, 10)
B = random.randint(1, 10)

print(f'{A} + {B} = ', add(A, B))
print(f'{A} - {B} = ', sub(A, B))
print(f'{A} * {B} = ', mul(A, B))
print(f'{A} / {B} = ', div(A, B))
print(f'{A} ^ (1/2) = ', square_root(num1=A))


get_string = input('Enter the String : ')
get_delim = input('Enter the delimiter : ')

returned_string = create_substring(string=get_string, delimiter=get_delim)
print('List of Substrings : ', returned_string)
