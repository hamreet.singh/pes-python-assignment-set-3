def biggest_of_4(a=0, b=0, c=0, d=0):
    if a > b and a > c and a > d:
        return a
    elif b > c and b > d:
        return b
    elif c > d:
        return c
    else:
        return d


print(biggest_of_4(10, 20, 40, 30))
print(biggest_of_4())
