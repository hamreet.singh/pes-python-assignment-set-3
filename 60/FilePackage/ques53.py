# Function 1
rf = open('file1.txt', 'r')

print('-' * 50)

# Function 2
print('File Number : ', rf.fileno())
print('-' * 50)

# Function 3
print('Reading 10 Characters : ', rf.read(10))
print('-' * 50)
print('Reading Remaining file characters : ', rf.read())
print('-' * 50)

# Function 4
print('Readable ? : ', rf.readable())
print('-' * 50)

# Function 5
rf.close()


with open('file2.txt', 'r') as rf:

    # Function 6
    line_list = rf.readlines()
    print('Reading File 2 : ', line_list)
    print('-' * 50)

    # Function 7
    print('Reading a single line from file 2 : ', rf.readline())
    print('-' * 50)

    # Function 8
    rf.seek(0)

    print('Reading a single line from file 2 : ', rf.readline(), end='')
    print('-' * 50)

    # Function 9
    print('Seekable ? : ', rf.seekable())
    print('-' * 50)

    # Function 10
    print('Current Position : ', rf.tell())
    print('-' * 50)

    # Function 11
    print('Writable : ', rf.writable())
    print('-' * 50)


with open('file2.txt', 'r') as rf:

    with open('file3.txt', 'w') as wf:
        date_from_file1 = rf.read()

        # Function 12
        wf.write(date_from_file1)

        print('File 3 is Written!')
        print('-' * 50)

with open('file4.txt', 'w') as wf:

    with open('file3.txt', 'r') as rf:

        for i in rf:
            # Function 13
            wf.writelines(rf.readlines())

    print('File 4 is Written!')
    print('-' * 50)
