tup1 = ('Lorem', 'ipsum', 'Lorem', 'bitcoin', 'sit', 'coin')
tup2 = (10, 15, 19, 8, 21, 15)

def iseven(value):
    if value % 2 == 0:
        return True
    return False

print('Tuple 1 : ', tup1)
print('Tuple 2 : ', tup2)

print('Count the occurences of Lorem : ', tup1.count('Lorem'))
print('Index of 19 : ', tup2.index(19))
print('Convert a list to tuple : ', tuple([1,2,3,4]))
print('Check for one True Value : ', any(tup1))
print('Check for all True Value : ', all(tup1))
print('Get string : ', ascii(tup1))
print('Length of the Tuple : ', len(tup1))
print('Change to Boolean : ', bool(tup1))
print('Enumerated Object of tuple 1 : ', enumerate(tup1))
print('Check For Even : ', list(filter(iseven, tup2)))
print('Convert to Iterator Object : ', iter(tup2))
print('Length of the Tuple : ', len(tup1))
print('Maximum Value in Tuple 2 : ', max(tup2))
print('Minimum Value in Tuple 2 : ', min(tup2))
print('Convert numbers to string : ', tuple(map(str, tup2)))
print('Reverse the Tuple 1 : ', tuple(reversed(tup1)))
print('Slice the Tuple 1: ', slice(tup1))
print('Sort the Tuple 2: ', sorted(tup2))
print('Sum of values of Tuple 2 : ', sum(tup2))
print('Tuple Iterator : ', tuple(zip(tup1)))
