try:
    first = int(input("Enter first number: "))
    second = int(input("Enter second number: "))
    print(first/second)
except ArithmeticError:
    print("Something wroing with the calculation part")
except KeyboardInterrupt:
    print("All right! Execution is halted manually")
except NameError:
    print("Variable name is not defined")
else:
    print("Program successfully executed!")
