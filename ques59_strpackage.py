import ques59_stringop as stp

while(True):
    print('1. Sort Numbers')
    print('2. Search a Number')
    print('3. Reverse String')
    print('4. Exit')

    try:
        choice = int(input('-->'))
    except ValueError:
        break

    if choice == 1:
        get_list = list(map(int, input('Enter a list of  Numbers (space separated) : ').split(' ')))
        print('Sorted List : ', stp.sort_number(get_list))

    elif choice == 2:
        get_list = list(map(int, input('Enter a list of  Numbers : ').split(' ')))
        get_num = int(input('Enter the number to search : '))

        print(stp.search_number(get_num, get_list))

    elif choice == 3:
        get_string = input('Enter a string to reverse : ')
        print('Reverse String : ', stp.reverse_string(get_string))

    else:
        break
