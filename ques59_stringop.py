def sort_number(num=[]):
    l = len(num)
    for i in range(l):
        for j in range(l):
            if num[j] > num[i]:
                temp = num[i]
                num[i] = num[j]
                num[j] = temp

    return num


def search_number(number, the_list=[]):

    the_list = sort_number(the_list)

    s = 0
    l = len(the_list) - 1

    while(s < l):
        mid = (l + s) // 2

        if number > the_list[mid]:
            s = mid + 1
        elif number < the_list[mid]:
            l = mid - 1
        else:
            return 'Found'

    return 'Not Found'


def reverse_string(string=''):
    return string[::-1]
