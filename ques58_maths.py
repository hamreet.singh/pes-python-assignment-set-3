from ques58_calc import fib, primenumber, mod

num1 = int(input('Enter a number : '))
num2 = int(input('Enter another number : '))


print(f'Fibonacci Series of {num1} numbers : ', fib(num1))

if primenumber(num1) == True:
    print(f'{num1} is a Prime Number')
else:
    print(f'{num1} is not a Prime Number')


print(f'{num1} % {num2} = {mod(num1, num2)}')
