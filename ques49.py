with open('file1.txt', 'r') as f:
	print('File 1 Contents : ', f.read())

with open('file2.txt', 'w') as f:
	f.writelines('This is a sample text 1\n')
	f.writelines('This is a sample text 2\n')
	f.writelines('This is a sample text 3\n')
	f.writelines('This is a sample text 4\n')
	f.writelines('This is a sample text 5\n')
	print('Writen Data to File 2!')

with open('file1.txt', 'a') as f:
	f.writelines('This is a sample text 1\n')
	f.writelines('This is a sample text 2\n')
	f.writelines('This is a sample text 3\n')
	f.writelines('This is a sample text 4\n')
	f.writelines('This is a sample text 5\n')
	print('Appended Data to File 1!')
